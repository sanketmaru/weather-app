# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Weather App built using AngularJS and openweatherdata API
* Angular 1.5.x, HighCharts , Underscore.js, Angular UI Router


### How do I get set up? ###

* Open index.html and you are up and running.
* No Configuration Required.
* Once index.html is viewed it will display list of cities along with its temp, wind.
* Click on More Weather to see forecast for next 5 days and the chart for it.

### Project Structure ###

* Project is divided into folders js,css, templates and has a main file which includes all the nexcessary files to load.

* js folders contains
  * Components - The main components of a web app
  * config - common configurations of project
  * services - Any common services which are used by complete app can be placed in this folder
  * shared - shared directives, services across the main components

* templates - templates folder should contain all the templates, currently it has none as all has been placed
              inside index.html so app runs without use of a web server. 

* css - styling files to be included here which should be modules/components based.

* utility - util folder contains files such as some string conversions, timezones related utils or any common functionality across the app

### Key Points ###

* Request Service is a single point of contact where all requests are done. It contains methods for single/batch requests.In future if decided to change to a different http service it can be easily changed, for example to $resource.

* The weather page and weather detail page contains a common weather directive which displays current weather of selected city. UI is better if viewed current weather is displayed along with the forecast for next 5 days.  

* Config Module contains app constants such as backend url's, appid and error messages in message-constants

* Moment.js included to solve date issues across browsers