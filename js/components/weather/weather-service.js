angular.module('Weather')
	.service('WeatherService', ['RequestService', 'AppConfig', 
	function(RequestService, AppConfig){

		var _getWeatherConfig = function(city, url) {
			return { 
				url : url,
				params :  { 
					q  : city ,
					appid : AppConfig.appid,
					mode : 'json'
				}
			}
		};

		/**
		* Get weather for all cities
		*/
		this.getWeatherByCities = function(cities){
			var configs = cities.map(function(city){
				return _getWeatherConfig(city, AppConfig.weatherUrl);
			});
			return RequestService.getAll(configs);
		};

		/**
		* Get weather by city
		*/
		this.getWeatherByCity = function(city){
			var config =  _getWeatherConfig(city, AppConfig.weatherForecastUrl);
			return RequestService.get(config);
		};

	}]);
