angular.module('Weather', ['RequestModule', 'Config'])
	.controller('WeatherCtrl', ['$scope','$state', 'WeatherService','UI_MESSAGE',
		function($scope, $state, WeatherService, Message){

		var _getWeatherByCities = function() {
			var cities = ['London', 'Amsterdam', 'Pune', 'Atlanta', 'Singapore']
			WeatherService.getWeatherByCities(cities)
				.then(function(response){					
					$scope.weathers =  _.pluck(response, 'data');;
				})
				.catch(function(err){
					alert(err);
				});
		}

    _getWeatherByCities();

		$scope.goToWeatherDetail = function(weather) {
			$state.go('weather-detail', {
				weather : weather
			});
		};
    
	}]);
