angular.module('Weather')
	.controller('WeatherDetailCtrl', ['$scope','$state', 'WeatherService','UI_MESSAGE',
		function($scope, $state, WeatherService, Message){

		var currentWeather = $state.params.weather || { name : 'London'};
		$scope.weathers = [currentWeather];

		var _getWeatherByCity = function() {
			
			WeatherService.getWeatherByCity(currentWeather.name)
				.then(function(response){
					var weather = response.data;
					var temp = [];
					weather.forecast = weather.list.filter(function(item){
						var weatherDate = moment(item.dt_txt);
						if(weatherDate.hours() === 12){
							temp.push(item.main.temp);
							return weather;
						}
					});
					$scope.cityWeather = weather;
					
					$scope.chart = {
						options: {
							chart: {
								type: 'spline'
							},
							xAxis: {
								categories: _.pluck($scope.cityWeather.forecast, 'dt_txt')
							},
							yAxis: {
								title: {
										text: 'Temperature'
								}
							}
						},
						title: {
								text: 'Temperature Chart'
						},
						series: [{
							data:  temp
						}]
					};
				})
				.catch(function(err){
					alert(err);
				});
		}

    _getWeatherByCity();

	}]);
