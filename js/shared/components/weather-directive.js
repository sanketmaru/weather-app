angular.module("SharedWeather", [])
	.directive("weather", function() {
		return {
			templateUrl : "weather.html",
			scope : {
				weathers : '=',
				more : '=',
				weatherDetail : '&' 
			},
			controller: function($scope) {
				$scope.goToWeatherDetail = function(weather) {
					$scope.weatherDetail({weather:weather});
				};
			},
		};
	});