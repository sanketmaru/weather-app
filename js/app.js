'use strict';
angular.module('Dashboard', [
	'ui.router',
	'highcharts-ng',
	'Weather',
	'SharedWeather',
	'Config',
	]).config(['$urlRouterProvider','$stateProvider',
  function($urlRouterProvider, $stateProvider) {

    $urlRouterProvider.otherwise("/weather");

	  $stateProvider
	    .state('weather', {
	      url: "/weather",
	      templateUrl: "home.html",
	      controller: "WeatherCtrl"
	    })
	    .state('weather-detail', {
	      url: "/weather-detail",
				params : {
					weather : {}
				},
				templateUrl: "weather-detail.html",
	      controller: "WeatherDetailCtrl"
	    });
  }
]);
