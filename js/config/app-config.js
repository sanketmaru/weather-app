angular.module('Config', [])
  .constant("AppConfig", (function() {
    var baseUrl = 'http://api.openweathermap.org/data/2.5/';
    return {
      weatherUrl : baseUrl + 'weather',
      weatherForecastUrl : baseUrl + 'forecast',
      appid : '3d8b309701a13f65b660fa2c64cdc517'
    }
  }()));
